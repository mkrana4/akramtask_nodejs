const express = require('express');
const router = express.Router();

const { registerUser, updateUser, getUser, getUsers, deleteUser } = require('../controllers/userController');

router.get('/', getUser);
router.post('/', registerUser);
router.delete('/', deleteUser);
router.put('/', updateUser);
router.get('/list', getUsers);

module.exports = router;