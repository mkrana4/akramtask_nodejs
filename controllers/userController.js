const userModel= require('../models/userModel');

let controller = {};

controller.registerUser = async (req, res, next) => {
  try {
    if(!req.body.email){
      return res.status(400).send({ success: false, messgae: "Email is required."})
    }
    let user = await userModel.findOne({email:req.body.email});
    if(user){
      return res.status(400).send({ success: false, messgae: "User with this email already exist."})
    }       
    await new userModel(req.body).save();
    res.status(200).send({ success: true, messgae: "Saved!"})
  } catch (error) {
    next(error);
  }
};


controller.updateUser = async (req, res, next) => {
  try {
    if(!req.body.userId){
      return res.status(400).send({ success: false, messgae: "User id is required."})
    }
    await userModel.findOneAndUpdate({_id: req.body.userId}, req.body);
    res.status(200).send({ success: true, messgae: "Updated!"})  
  } catch (error) {
    next(error);
  }
};

controller.deleteUser = async (req, res, next) => {
  try {
    if(!req.body.userId){
      return res.status(400).send({ success: false, messgae: "User id is required."})
    }
    await userModel.deleteOne({_id: req.body.userId});
    res.status(200).send({ success: true, messgae: "Deleted!"})  
  } catch (error) {
    next(error);
  }
};

controller.getUser = async (req, res, next) => {
  try {
    let resp = await userModel.findOne(req.query);       
    res.status(200).json({success: true, data: resp});
  } catch (error) {
      next(error);
  }
};

controller.getUsers = async (req, res, next) => {
  let resp = await userModel.find(req.query);
  res.status(200).json({success: true, data: resp});
};


module.exports = controller;