const mongoose = require('mongoose');
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
const SERVER_PORT = process.env.PORT || 3000;

app.use((req, res, next)=>{
    console.log("API hitted", req.url);
    next();
})

app.use('/api/user', require('./route/userRoute'));


mongoose.connect('mongodb://127.0.0.1:27017/akramTask', {
    useNewUrlParser: true,
    useCreateIndex: true
});

app.listen(SERVER_PORT, function () {
    console.log('server connected ' + SERVER_PORT);
});


app.use((err,req,res,next) =>{
    console.log("Error--->", err);
    return res.status(500).json({status : "error", message: "Unhandled error" });
})