const MONGOOSE = require('mongoose');
const Schema = MONGOOSE.Schema;

const User = new Schema({
    firstname: { type: String, default: "F" },
    lastname: { type: String, default: "L" },
    age: { type: Number, default:18 },
    gender: { type: String, enum: ['male', 'female'], default: 'male' },
    email: { type: String, required: true, email: true, unique: true }
}, { timestamps: true, versionKey: false});

module.exports = MONGOOSE.model('users', User);